# Analyses for taxonomy courpus

This repository contains analysis scripts used for particular analyses of the
taxonomy corpus at Sciveyor. These scripts are customized for the particular
questions raised by that dataset, though they rely upon models and corpora
generated in the format created by my
[quick-topic](https://codeberg.org/cpence/quick-topic) scripts.

These scripts require that `quick-topic` is importable. Since I'm not packaging
that package for public consumption yet, the quickest thing to do is `export PYTHONPATH="path/to/quick-topic/repo/"`. Yes, I know, this is a hack and sucks,
I'll fix it when I'm not behind on a deadline.

# Scripts

## Correlation Data

- `topic-by-journal`: Print out "average" topic weight for every article in each
  journal in the corpus, as well as the mutual-information for topics x
  journals.
- `topic-by-taxon`: Same as the above, but for documents that mention each of
  fourteen different large taxa (some of which are in fact colloquial
  polyphyletic groups; see code for more info).
- `topic-by-concept`: Same as the above, but for documents that mention each of
  a list of twenty species concepts; see code for the list. This script requires
  `flashtext` to be installed in the venv.
- `concept-by-taxon`: Print out the counts of documents that mention each
  combination of taxon and species concept, as well as the mutual-information
  for concepts x taxa.

## Geocoding

- `strip-refs-affils`: Geocoding often produces very noisy output because the
  references and affiliations are included in the article plaintext. This is a
  collection of hacks, _just_ for the journals included in our dataset, that
  tries to remove references and affiliations from articles. My
  back-of-the-envelope testing indicates that it's something like 90-95%
  accurate for our corpus.
- `run-stanford-ner`: Run the Stanford CoreNLP Named Entity Recognizer against
  all of the clean-txt files produced by the `strip-refs-affils` script. This
  will create JSON files in the current folder; these were then manually renamed
  to a `.ner` file extension.
- `extract-locations`: Parse the output files from the Stanford NER to produce
  plain-text files containing only a list of all `LOCATION` type entity mentions
  in each paper. Those are stored in files with a `.locations` extension.
- `eval_geocoding/*`: A set of scripts to send a sample set of 1,000 random
  locations drawn from our dataset to a number of different geocoding providers
  so that we could try to evaluate their quality. We settled on Google.

# License

As with `quick-topic`, these scripts are licensed under GPL3.
