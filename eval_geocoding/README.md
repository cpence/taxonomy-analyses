# Comparison of Geocoding Services

This folder contains a comparison of the following geocoding services:

- Bing Spatial Data
- Geoapify
- GeocodeEarth
- Google Maps
- HERE
- LocationIQ
- OpenCage

## The Data

We have a peculiar kind of dataset: it's the results of running the Stanford NER
against a massive corpus of scientific articles. Most geocoding services seem to
be designed to take users' best-faith attempts to type real-world addresses or
the names of places that, you know, actually exist, and try to turn those into
proper, standardized locations. Our data is different from that in at least the
following ways:

- Extremely dirty: Lots of the "locations" that we're asked to match are not
  locations at all. The Stanford NER tends to see species names as locations
  (e.g., in the sample data, things like "Euryphthiria" or "Clypeodytes"), as
  well as sometimes catching multiple locations in a row and returning them as a
  single hit (e.g., in the sample data, "Philippines Indonesia Indonesia
  Indonesia Brazil").
- Includes regions and areas in addition to addresses: We'll get everything from
  point-locations like "Montagne de la Gardiole" to extended locations like "Rio
  Hondo"or "Rocky Mts." to entire regions like "South Western Tasmania" or
  "Guangzxi Province of Southwest China".
- Includes natural features: We've also got non-political features like rivers,
  lakes, forests, seas, and parks. We'd like to be sure that those are matched
  as well.

That means that we needed to run our own tests, to see how things do. I pulled a
sample of 1,000 locations from the journal articles taken as a whole. (These are
in `test.locations`, one location per line.) I then passed that sample through
each of the seven geocoding services listed above.

## Manual Coding

I then reviewed all 1,000 of the given locations and categorized them into three
types: (1) definitely a location, (2) possibly a location (largely for things in
unusual languages, misspellings, larger regions/rivers/areas, or ambiguous
location or road names), and (3) definitely not a location. (These are in
`manual.locations`, with YES, NO, and MAYBE on each line.) Of course, I don't
claim that these are perfect: they're largely the result of some best guesses
and Googling. But at least they can serve as a partial benchmark.

That gives us four pieces of data for each provider:

- Overall rate of locations matched (difficult to evaluate, largely informative)
- Failure rate on genuine locations (percentage of missed YES locations; low is
  good)
- Match rate on possible locations (percentage of matched MAYBE locations;
  difficult to evaluate, largely informative)
- False-positive rate on non-locations (percentage of matched NO locations; low
  is good)

## Results

In the end, our random sample contained:

- 444 confirmed-good locations
- 200 potential locations
- 356 non-locations

This 35% noise rate is roughly as expected given the mixed quality of the
Stanford NLP data when applied to sources containing taxonomic names. The
performance of each of the location data providers is summed up in the following
table:

| Data Source  | Overall Rate | Failure Rate | Maybe Rate | False-Positive Rate |
| :----------- | -----------: | -----------: | ---------: | ------------------: |
| Bing         |        70.2% |     **7.7%** |      88.5% |               32.6% |
| Geoapify     |        68.8% |         9.5% |      81.5% |               34.6% |
| GeocodeEarth |        71.0% |     **7.7%** |      87.0% |               35.4% |
| Google Maps  |        56.1% |          18% |      66.0% |           **18.2%** |
| HERE         |        75.1% |         9.6% |      85.0% |             _50.6%_ |
| LocationIQ   |        66.0% |        11.9% |      74.5% |               33.7% |
| Open Cage    |        63.2% |        16.7% |      74.5% |               31.7% |

Observations:

1. We can immediately see that HERE is disqualified for our purposes; its
   matcher is _extremely_ greedy and produces an unreasonable rate of false
   positives in our data. (HERE does provide a result quality score; filtering
   with this parameter doesn't help.)
2. Other than Google Maps, _all_ providers return nonsense false positives for
   around one-third of our non-location entries.
3. Google Maps, on the contrary, has double the false-negative rate of the best
   providers (Bing and GeocodeEarth), and a significantly lower "Maybe" rate.
4. Taken together, and perhaps unsurprisingly, this looks to be a two-horse race
   between Bing and GeocodeEarth (roughly interchangeable, lower false-negative,
   higher false-positive) and Google (lower false-positive, higher
   false-negative).
5. I kept GeocodeEarth in the following because they have clearer and less
   annoying licensing/pricing structures.

To break the tie, it is important to remember _why_ we are worried about false
positives and false negatives. False negatives, of course, could lead to missing
coverage. But false positives would -- given the overall bias of these kinds of
datasets to locations in North America and Western Europe -- likely bias the map
in favor of those locations with respect to the rest of the world. To examine
this possibility, I then mapped all of the locations found in both the Google
and GeocodeEarth results.

As perhaps expected, the GeocodeEarth results show significantly higher
concentrations in population centers in the US and Europe, especially the East
and West Coast and the UK and Germany, likely largely a result of false
positives. Google results are significantly more minimal in those regions; North
American points are found more often in wilderness areas.

In short: for our dataset, Google is the clear geocoding winner.
