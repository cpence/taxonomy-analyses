from flashtext import KeywordProcessor
from tqdm import tqdm
import json

# A list of all named species concepts to search for (based on work of Stijn
# Conix, drawing from the book of Frank Zachos)
SPECIES_CONCEPTS = [
    "phylo-phenetic species concept",
    "phylogenetic species concept",
    "genic species concept",
    "cohesion species concept",
    "genealogical concordance species concept",
    "genotypic cluster species concept",
    "genetic species concept",
    "ecological species concept",
    "recognition species concept",
    "genealogical species concept",
    "biological species concept",
    "differential fitness species concept",
    "compilospecies concept",
    "cladistic species concept",
    "hennigian species concept",
    "internodal species concept",
    "mitonuclear compatibility species concept",
    "pragmatic species concept",
    "inclusive species concept",
    "biosimilarity species concept",
]

# Get species concepts in every document. Returns an array of lists of unique
# strings drawn from SPECIES_CONCEPTS above, one for each document in the
# corpus.
def get_document_concepts(corpus_filenames):
    proc = KeywordProcessor()
    for c in SPECIES_CONCEPTS:
        proc.add_keyword(c)

    ret = []
    for fn in tqdm(corpus_filenames):
        with open(fn) as f:
            data = json.load(f)
            ft = data.get("fullText")

        if ft is None:
            continue
        if len(ft) == 0:
            continue

        concepts = proc.extract_keywords(ft)
        ret.append(list(set(concepts)))

    return ret
