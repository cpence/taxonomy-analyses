from math import log2, fabs, pow

# Add an argument for picking an MI method
def add_args(parser):
    parser.add_argument(
        "--mi",
        "-i",
        default="pmi",
        choices=["pmi", "npmi", "pmi_2", "npmi_2", "pmi_3", "npmi_3"],
        help="mutual information estimate to use [default: pmi, also available npmi, pmi_2, npmi_2, pmi_3, npmi_3]",
        metavar="ALG",
    )


# Get the pointwise mutual information, in the sense familiar from search
# engines and digital humanities:
#
# log(Pr(A & B) / (Pr(A) * Pr(B)))
def pmi(pr_ab, pr_a, pr_b):
    # Make sure that we don't NaN for very rare things
    if fabs(pr_a) < 0.0001 or fabs(pr_b) < 0.0001 or fabs(pr_ab) < 0.0001:
        return 0.0

    return log2(pr_ab / (pr_a * pr_b))


# Get the normalized pointwise mutual information, which ranges only from [-1,
# 1] with 0 being uncorrelated:
#
# pmi(pr_ab, pr_a, pr_b) / -log(Pr(A & B)
#
# Ref: https://svn.spraakdata.gu.se/repos/gerlof/pub/www/Docs/npmi-pfd.pdf
def npmi(pr_ab, pr_a, pr_b):
    return pmi(pr_ab, pr_a, pr_b) / -log2(pr_ab)


# Get the PMI^k measure, which can help to correct the bias of PMI toward
# low-frequency events:
#
# log(Pr(A & B)^k / (Pr(A) * Pr(B)))
#
# Values of k=2 and k=3 (especially k=3) have been tested successfully in
# digital humanities ontexts. Ref:
# https://www.researchgate.net/publication/282359004_Handling_the_Impact_of_Low_Frequency_Events_on_Co-occurrence_based_Measures_of_Word_Similarity_-_A_Case_Study_of_Pointwise_Mutual_Information
def pmi_k(k, pr_ab, pr_a, pr_b):
    # Make sure that we don't NaN for very rare things
    if fabs(pr_a) < 0.0001 or fabs(pr_b) < 0.0001 or fabs(pr_ab) < 0.0001:
        return 0.0

    return log2(pow(pr_ab, k) / (pr_a * pr_b))


# Get the normalized PMI^k
def npmi_k(k, pr_ab, pr_a, pr_b):
    return pmi_k(k, pr_ab, pr_a, pr_b) / -log2(pr_ab)


# Get the MI requested by the user
def pmi_for(args, pr_ab, pr_a, pr_b):
    arg = args.mi
    if arg == "npmi":
        return npmi(pr_ab, pr_a, pr_b)
    elif arg == "pmi_2":
        return pmi_k(2.0, pr_ab, pr_a, pr_b)
    elif arg == "npmi_2":
        return npmi_k(2.0, pr_ab, pr_a, pr_b)
    elif arg == "pmi_3":
        return pmi_k(3.0, pr_ab, pr_a, pr_b)
    elif arg == "npmi_3":
        return npmi_k(3.0, pr_ab, pr_a, pr_b)
    else:
        return pmi(pr_ab, pr_a, pr_b)


# Return a description of the PMI calculation used to put in the output
def pmi_desc(args):
    arg = args.mi
    if arg == "npmi":
        return "NPMI"
    elif arg == "pmi_2":
        return "PMI^2"
    elif arg == "npmi_2":
        return "NPMI^2"
    elif arg == "pmi_3":
        return "PMI^3"
    elif arg == "npmi_3":
        return "NPMI^3"
    else:
        return "PMI"
