from tqdm import tqdm
from os.path import splitext, exists
import json

# "Categories" are a crude vernacular reduction of taxonomy to a manageable
# number of big "groups", some of which are proper, monophyletic groups (birds,
# mammals, molluscs) and some of which are messy, common-language things (algae,
# protists). This is a way to do some coarse-grained analysis between "groups"
# and journals or topics, without having to choose, say, "classes" as the sole
# level of analysis all across the tree of life.

# Algae are a polyphyletic mess; we start by just trying to match against *any*
# of these taxon names which are usually used to distinguish them
ALGAE_TAXA = {
    "Mesostigmatophyceae",
    "Chlorophyta",
    "Charophyta",
    "Chlorokybophyceae",
    "Chlorokybales",
    "Chlorokybaceae",
    "Chlorokybus",
    "Rhodophyta",
    "Glaucophyta",
    "Chlorarachniophyceae",
    "Euglenophyceae",
    "Bacillariophyceae",
    "Actinochrysophyceae",
    "Triparma",
    "Triparmaceae",
    "Parmales",
    "Bolidophyceae",
    "Eustigmatophyceae",
    "Phaeophyceae",
    "Chrysophyceae",
    "Raphidophyceae",
    "Synurales",
    "Synurophyceae",
    "Xanthophyceae",
    "Cryptophyceae",
    "Dinoflagellata",
    "Haptophytina",
}

# Same thing with protists; we just list a bunch of recognized taxa that aren't
# themselves algae
PROTIST_TAXA = {
    "Breviata",
    "Breviatidae",
    "Breviatida",
    "Breviatea",
    "Excavata",
    "Metamonada",
    "Percolozoa",
    "Discicristata",
    "Discoba",
    "Amoebozoa",
    "Apusomonadidae",
    "Apusomonadida",
    "Apusomonadea",
    "Hacrobia",
    "Chromista",
    "Hemimastigophora",
    "Choanoflagellatea",
    "Cristidiscoidea",
    "Filasterea",
    "Mesomycetozoea",
    "Pluriformea",
    "SAR",
    "Harosa",
    "Heterokonta",
    "Alveolata",
    "Halvaria",
    "Apicomplexa",
    "Ciliophora",
    "Rhizaria",
    "Cercozoa",
    "Foraminifera",
    "Retaria",
    "Ectoreta",
    "Radiolaria",
    "Oomycetes",
    "Hyphochytriomycota",
    "Hyphochytriomycetes",
    "Hyphochytriomycetidae",
    "Hyphochytriales",
    "Bigyromonadea",
    "Bicosoecida",
    "Bikosea",
    "Cyathobodoniae",
    "Bicosidia",
    "Labyrinthulomycetes",
    "Blastocystis",
    "Blastocystidae",
    "Blastocystida",
    "Blastocystae",
    "Ochrophyta",
    "Khakista",
    "Bacillariophyceae",
    "Chrysomerophyceae",
    "Chrysomeridales",
    "Chrysomeridaceae",
    "Dictyochophyceae",
    "Pelagophyceae",
    "Phaeothamniophyceae",
    "Pinguiophyceae",
    "Pinguiochrysidales",
    "Pinguiochrysidaceae",
    "Synchromophyceae",
    "Protozoa",
}

# Fish are split across a few taxa
FISH_TAXA = {"Dipnoi", "Cyclostomi", "Actinistia", "Actinopterygii", "Chondrichthyes"}

# Some data sources seem not to use "Reptilia", preferring the other ways to
# divide the reptiles; we need to add a lot of those.
REPTILE_TAXA = {
    "Reptilia",
    "Amphibia",
    "Lepidosauromorpha",
    "Lepidosauria",
    "Rhynchocephalia",
    "Squamata",
    "Archelosauria",
    "Sauria",
    "Pantestudines",
    "Testudinata",
    "Perichelydia",
    "Testudines",
    "Archosauria",
    "Eucrocopoda",
    "Crocodilia",
    "Eusuchia",
    "Crocodylomorpha",
    "Pseudosuchia",
}

# What we have here is a set of path elements, from kingdom to however far down
# the list it goes. Map this into our crude categories. Note that you need to
# pass this function a set!
#
# Returns None if we don't know how to categorize the given path.
def path_to_category(path):
    # Algae are a polyphyletic mess; we start with these guys first by seeing if
    # they match any of a list drawn from Wikipedia
    if path & ALGAE_TAXA:
        return "Algae"
    # If they're protists but not algae, return "Protist"
    if path & PROTIST_TAXA:
        return "Protists"

    # Easy groups
    if "Mammalia" in path:
        return "Mammals"
    if "Aves" in path:
        return "Birds"
    if path & REPTILE_TAXA:
        # Note: This also includes the birds, but they have already been
        # filtered out
        return "Reptiles and Amphibia"
    if path & FISH_TAXA:
        return "Fish"
    if path & {"Ambulacraria", "Echinodermata", "Hemichordata"}:
        return "Non-vertebrate Deuterostomes"
    if "Insecta" in path:
        return "Insects"
    if "Arthropoda" in path:
        # Note: This also includes the insects, but they have already been
        # filtered out
        return "Non-insect Arthropods"
    if "Mollusca" in path:
        return "Molluscs"
    if "Animalia" in path and "Vertebrata" not in path:
        return "Other Invertebrates"
    if path & {"Plantae", "Streptophyta", "Embryophytes"}:
        # Note: This also includes the algae, but they have already been
        # filtered out
        return "Plants"
    if "Fungi" in path:
        return "Fungi"
    if path & {"Bacteria", "Archaea"}:
        return "Prokaryotes"

    return None


# Parse the document taxa files and reduce them to a list of our large-scale taxon categories
def get_document_taxa(corpus_filenames, debug=False):
    ret = []
    unverified_count = 0
    poor_data_count = 0
    unknown_count = 0
    good_count = 0

    for json_file in tqdm(corpus_filenames):
        out = set()

        # Change the file extension
        name, ext = splitext(json_file)
        taxa_file = name + ".taxa.json"

        # Bounce if we don't even have a taxa file
        if not exists(taxa_file):
            ret.append(out)
            continue

        with open(taxa_file) as f:
            taxa = json.load(f)
        if "names" not in taxa:
            ret.append(out)
            continue

        for name in taxa["names"]:
            # Okay, we really only want one thing in each name, drill down for it
            # Sadly, if we don't have classificationPath we just can't use a hit; we
            # need to be able to climb the ToL to see where to put things.
            if "verification" not in name:
                unverified_count += 1
                continue

            ver = name["verification"]
            if "bestResult" not in ver:
                unverified_count += 1
                continue

            res = ver["bestResult"]
            if "classificationPath" not in res:
                poor_data_count += 1
                continue

            cp = res["classificationPath"]
            path_elts = set(cp.split("|"))
            path_elts.discard("")

            category = path_to_category(path_elts)
            if category is None:
                unknown_count += 1
                continue

            good_count += 1
            out.add(category)

        ret.append(list(out))

    total_count = unverified_count + poor_data_count + unknown_count + good_count

    if debug:
        print("")
        print("Name-Match Statistics:")
        print(f"- total hits in all articles: {total_count}")
        print(f"- unverified hits in text: {unverified_count * 100 / total_count}%")
        print(f"- not enough data: {poor_data_count * 100 / total_count}%")
        print(f"- unknown category: {unknown_count * 100 / total_count}%")
        print(f"- successfully categorized: {good_count * 100 / total_count}%")
        print("")
        print("")

    return ret


# A list of all the categories that might be returned by get_document_taxa.
CATEGORIES = [
    "Algae",
    "Protists",
    "Mammals",
    "Birds",
    "Reptiles and Amphibia",
    "Fish",
    "Non-vertebrate Deuterostomes",
    "Insects",
    "Non-insect Arthropods",
    "Molluscs",
    "Other Invertebrates",
    "Plants",
    "Fungi",
    "Prokaryotes",
]
