#!/usr/bin/env python
import argparse
from quick_topic import corpus_args, model_args
from shared.taxa import get_document_taxa
from shared import mi
from tqdm import tqdm

parser = argparse.ArgumentParser(
    description="Calculate the relationship between topics and large taxon categories"
)
corpus_args.add(parser)
mi.add_args(parser)
model_args.add(parser)
args = parser.parse_args()

# Load the model
corpus_filenames = corpus_args.load_corpus_filenames(parser, args, required=True)
lda = model_args.load(parser, args, required=True)
document_topics = model_args.load_document_topics(parser, args, required=True)

num_docs = len(document_topics)
num_topics = lda.num_topics

# Get the taxa in each document
document_taxa = get_document_taxa(corpus_filenames, debug=True)

# Do the correlation
taxa_topics = {}  # Pr(topic | document-mentions-taxon)
taxa_size = {}  # num of documents that mention each taxon
taxa_prob = {}  # fraction of documents that mention each taxon
topic_prob = [0.0] * num_topics  # fraction of overall prob mass in each topic

for i, doc in tqdm(enumerate(document_topics)):
    for pair in doc:
        topic_prob[pair[0]] += pair[1]

    taxa = document_taxa[i]
    for taxon in taxa:
        if taxon not in taxa_topics:
            taxa_topics[taxon] = [0.0] * num_topics
            taxa_size[taxon] = 0

        taxa_size[taxon] += 1

        for pair in doc:
            taxa_topics[taxon][pair[0]] += pair[1]

# Divide by total number of documents
topic_prob = [p / num_docs for p in topic_prob]

for t, array in tqdm(taxa_topics.items()):
    taxa_topics[t] = [p / taxa_size[t] for p in array]
    taxa_prob[t] = taxa_size[t] / num_docs

# Print results
print("Average Pr(t | doc) for all docs mentioning taxon:")
for taxon, topics in tqdm(taxa_topics.items()):
    print(f"\"{taxon} (N = {taxa_size[taxon]})\",{','.join([str(t) for t in topics])}")

print("")
print("")

# Compute mutual information for topics x taxa
# log( Pr(A & B) / (Pr(A) Pr(B)) )
print(f"Mutual information for topics x taxa ({mi.pmi_desc(args)}):")
for taxon, array in tqdm(taxa_topics.items()):
    out = []
    tax_prob = taxa_prob[taxon]
    for i, joint_prob in enumerate(array):
        top_prob = topic_prob[i]
        out.append(mi.pmi_for(args, joint_prob, top_prob, tax_prob))

    print(f"\"{taxon} (N = {taxa_size[taxon]})\",{','.join([str(x) for x in out])}")
