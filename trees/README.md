# Tree Visualizations

The tree visualization code here was used to generate a set of figures for a book chapter on the construction of our dataset. Basic information about their use:

1. First, run `gnfinder` to extract the taxonomic names from the corpus, using a
   command like

   ```sh
   gnfinder -f pretty -U -u -v -l eng $textfile > output.taxa.json
   ```

2. Run the script here to turn those names into Open Tree of Life taxonomy
   identifiers (`gnfinder_to_ott`).
3. Run another script to turn those OTT identifiers into subtrees of the Open
   Tree of Life (`subtree_for_names`).
4. If there are too many nodes in the tree to be able to visualize it using the
   iTOL Viewer, you can run `squash_leaves`, which collapses nodes with multiple
   leaves into a single leaf, then removes one layer of leaf nodes from the tips
   of the tree. (For instance, running this four times on the full Open Tree of
   Life tree, which can't be visualized, results in a tree with around 300,000
   leaves, which can be visualized.)
5. The annotations of large clades that we used for that visualization were:
   - Bacteria - ott844192 - #e69f00
   - Archaea - ott996421 - #56b4e9
   - Fungi - ott352914 - #f0e442
   - Plants (Chloroplastida) - ott361838 - #009e73
   - Molluscs + Annelids - mrcaott56ott519 - #0072b2
   - Arthropods - ott632179 - #d55e00
   - Chordates - ott125642 - #cc79a7
